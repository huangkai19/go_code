//告诉我们文件属于哪个包
//每个独立运行的go程序都必须包含package
package main
//导入fmt包
import "fmt"

//在main这个包中都必须包含一个main函数
func main() {
	//调用fmt包中的printf方法打印一段内容
	fmt.Printf("Hello World")
}